package com.nkl.page.domain;

import com.nkl.common.domain.BaseDomain;

public class House extends BaseDomain {

	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = -6925524708882684408L;
	private int house_id; // 
	private String house_no; // 
	private int house_type_id; // 
	private String house_desc; // 
	private int house_flag; // 1:空房 2:已预订 3:停用 4:已入住 5:待清理
	
	private String house_type_name; // 
	private double house_price; //
	private String ids;

	public String getHouse_flagDesc(){
		switch (house_flag) {
		case 1:
			return "空房";
		case 2:
			return "已预订";
		case 3:
			return "停用";
		case 4:
			return "已入住";
		case 5:
			return "待清理";
		default:
			return "";
		}
	}

	public int getHouse_id() {
		return house_id;
	}

	public String getHouse_no() {
		return house_no;
	}

	public int getHouse_type_id() {
		return house_type_id;
	}

	public String getHouse_desc() {
		return house_desc;
	}

	public int getHouse_flag() {
		return house_flag;
	}

	public String getHouse_type_name() {
		return house_type_name;
	}

	public String getIds() {
		return ids;
	}

	public void setHouse_id(int house_id) {
		this.house_id = house_id;
	}

	public void setHouse_no(String house_no) {
		this.house_no = house_no;
	}

	public void setHouse_type_id(int house_type_id) {
		this.house_type_id = house_type_id;
	}

	public void setHouse_desc(String house_desc) {
		this.house_desc = house_desc;
	}

	public void setHouse_flag(int house_flag) {
		this.house_flag = house_flag;
	}

	public void setHouse_type_name(String house_type_name) {
		this.house_type_name = house_type_name;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public double getHouse_price() {
		return house_price;
	}

	public void setHouse_price(double house_price) {
		this.house_price = house_price;
	}
}
