package com.nkl.page.domain;

import com.nkl.common.domain.BaseDomain;
import com.nkl.common.util.DateUtil;
import com.nkl.common.util.StringUtil;

public class Orders extends BaseDomain {

	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = -6925524708882684408L;
	private int orders_id; // 
	private int member_id; // 
	private String member_name; // 
	private String member_phone; // 
	private String member_card; // 
	private int house_id; // 
	private String stay_date; // 
	private int stay_days; // 
	private int orders_flag; // 1:未确认 2:未入住 3:已入住 4:已取消
	private String orders_date; // 

	private String house_no; // 
	private int house_type_id; // 
	private String house_type_name; //
	private double house_price; // 
	private String orders_date_min;
	private String orders_date_max;
	private String leave_date; // 
	private double stay_money; // 
	private String orders_flags;
	private String ids;

	public String getOrders_flagDesc(){
		switch (orders_flag) {
		case 1:
			return "未确认";
		case 2:
			return "未入住";
		case 3:
			return "已入住";
		case 4:
			return "已取消";
		default:
			return "";
		}
	}
	
	
	public int getOrders_id() {
		return orders_id;
	}

	public int getMember_id() {
		return member_id;
	}

	public String getMember_name() {
		return member_name;
	}

	public String getMember_phone() {
		return member_phone;
	}

	public int getHouse_id() {
		return house_id;
	}

	public String getStay_date() {
		return stay_date;
	}

	public int getStay_days() {
		return stay_days;
	}

	public int getOrders_flag() {
		return orders_flag;
	}

	public String getOrders_date() {
		return orders_date;
	}

	public String getHouse_no() {
		return house_no;
	}

	public int getHouse_type_id() {
		return house_type_id;
	}

	public String getHouse_type_name() {
		return house_type_name;
	}

	public String getOrders_date_min() {
		return orders_date_min;
	}

	public String getOrders_date_max() {
		return orders_date_max;
	}

	public String getIds() {
		return ids;
	}

	public void setOrders_id(int orders_id) {
		this.orders_id = orders_id;
	}

	public void setMember_id(int member_id) {
		this.member_id = member_id;
	}

	public void setMember_name(String member_name) {
		this.member_name = member_name;
	}

	public void setMember_phone(String member_phone) {
		this.member_phone = member_phone;
	}

	public void setHouse_id(int house_id) {
		this.house_id = house_id;
	}

	public void setStay_date(String stay_date) {
		this.stay_date = stay_date;
	}

	public void setStay_days(int stay_days) {
		this.stay_days = stay_days;
	}

	public void setOrders_flag(int orders_flag) {
		this.orders_flag = orders_flag;
	}

	public void setOrders_date(String orders_date) {
		this.orders_date = orders_date;
	}

	public void setHouse_no(String house_no) {
		this.house_no = house_no;
	}

	public void setHouse_type_id(int house_type_id) {
		this.house_type_id = house_type_id;
	}

	public void setHouse_type_name(String house_type_name) {
		this.house_type_name = house_type_name;
	}

	public void setOrders_date_min(String orders_date_min) {
		this.orders_date_min = orders_date_min;
	}

	public void setOrders_date_max(String orders_date_max) {
		this.orders_date_max = orders_date_max;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}


	public String getMember_card() {
		return member_card;
	}


	public void setMember_card(String member_card) {
		this.member_card = member_card;
	}


	public String getLeave_date() {
		if (StringUtil.isEmptyString(leave_date)) {
			return DateUtil.dateToDateString(DateUtil.getDateAfter(DateUtil.getDate(stay_date), stay_days));
		}
		return leave_date;
	}


	public double getStay_money() {
		return stay_money;
	}


	public void setLeave_date(String leave_date) {
		this.leave_date = leave_date;
	}


	public void setStay_money(double stay_money) {
		this.stay_money = stay_money;
	}


	public String getOrders_flags() {
		return orders_flags;
	}


	public void setOrders_flags(String orders_flags) {
		this.orders_flags = orders_flags;
	}


	public double getHouse_price() {
		return house_price;
	}


	public void setHouse_price(double house_price) {
		this.house_price = house_price;
	}
	
	 
}
