package com.nkl.page.domain;

import com.nkl.common.domain.BaseDomain;
import com.nkl.common.util.DateUtil;

public class Stay extends BaseDomain {
	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = 4031616815162168487L;
	private int stay_id; // 
	private int house_id; // 
	private int member_id; // 
	private String member_name; // 
	private String member_card; // 
	private String member_phone; // 
	private String stay_date; // 
	private String leave_date; // 
	private double stay_money; // 
	private double other_money; // 
	private double all_money; // 
	private int stay_flag; // 1:入住 2:退房

	private String house_no; // 
	private int house_type_id; // 
	private String house_type_name; //
	private double house_price; //
	private String stay_date_min;
	private String stay_date_max;
	private String ids;
	private String random;

	public String getOrders_flagDesc(){
		switch (stay_flag) {
		case 1:
			return "入住";
		case 2:
			return "退房";
		default:
			return "";
		}
	}
	
	public void setStay_id(int stay_id){
		this.stay_id=stay_id;
	}

	public int getStay_id(){
		return stay_id;
	}

	public void setHouse_id(int house_id){
		this.house_id=house_id;
	}

	public int getHouse_id(){
		return house_id;
	}

	public void setMember_id(int member_id){
		this.member_id=member_id;
	}

	public int getMember_id(){
		return member_id;
	}

	public void setMember_name(String member_name){
		this.member_name=member_name;
	}

	public String getMember_name(){
		return member_name;
	}

	public void setMember_card(String member_card){
		this.member_card=member_card;
	}

	public String getMember_card(){
		return member_card;
	}

	public void setStay_date(String stay_date){
		this.stay_date=stay_date;
	}

	public String getStay_date(){
		return stay_date;
	}

	public void setLeave_date(String leave_date){
		this.leave_date=leave_date;
	}

	public String getLeave_date(){
		return leave_date;
	}

	public void setStay_money(double stay_money){
		this.stay_money=stay_money;
	}

	public double getStay_money(){
		return stay_money;
	}

	public void setOther_money(double other_money){
		this.other_money=other_money;
	}

	public double getOther_money(){
		return other_money;
	}

	public void setAll_money(double all_money){
		this.all_money=all_money;
	}

	public double getAll_money(){
		if (all_money==0) {
			int days = (int)DateUtil.getApartDays(DateUtil.getDate(leave_date), DateUtil.getDate(stay_date));
			all_money = house_price * days + other_money;
		}
		return all_money;
	}

	public void setStay_flag(int stay_flag){
		this.stay_flag=stay_flag;
	}

	public int getStay_flag(){
		return stay_flag;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getIds() {
		return ids;
	}

	public void setRandom(String random) {
		this.random = random;
	}

	public String getRandom() {
		return random;
	}

	public String getHouse_no() {
		return house_no;
	}

	public int getHouse_type_id() {
		return house_type_id;
	}

	public String getHouse_type_name() {
		return house_type_name;
	}

	public double getHouse_price() {
		return house_price;
	}

	public void setHouse_no(String house_no) {
		this.house_no = house_no;
	}

	public void setHouse_type_id(int house_type_id) {
		this.house_type_id = house_type_id;
	}

	public void setHouse_type_name(String house_type_name) {
		this.house_type_name = house_type_name;
	}

	public void setHouse_price(double house_price) {
		this.house_price = house_price;
	}

	public String getStay_date_min() {
		return stay_date_min;
	}

	public String getStay_date_max() {
		return stay_date_max;
	}

	public void setStay_date_min(String stay_date_min) {
		this.stay_date_min = stay_date_min;
	}

	public void setStay_date_max(String stay_date_max) {
		this.stay_date_max = stay_date_max;
	}

	public String getMember_phone() {
		return member_phone;
	}

	public void setMember_phone(String member_phone) {
		this.member_phone = member_phone;
	}

}
