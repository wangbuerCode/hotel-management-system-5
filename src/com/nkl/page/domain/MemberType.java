package com.nkl.page.domain;

import com.nkl.common.domain.BaseDomain;

public class MemberType extends BaseDomain {
	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = 1770185824735782580L;
	private int member_type_id; // 
	private String member_type_name; // 

	private String ids;

	public int getMember_type_id() {
		return member_type_id;
	}

	public void setMember_type_id(int member_type_id) {
		this.member_type_id = member_type_id;
	}

	public String getMember_type_name() {
		return member_type_name;
	}

	public void setMember_type_name(String member_type_name) {
		this.member_type_name = member_type_name;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

}
