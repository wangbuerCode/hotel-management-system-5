package com.nkl.page.domain;

import com.nkl.common.domain.BaseDomain;

public class Member extends BaseDomain {
	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = -4828584948238496474L;
	private int member_id; // 
	private String member_no; // 
	private String member_name; // 
	private int member_type_id; // 
	private String member_card; // 
	private int member_sex; // 1���� 2��Ů
	private int member_age; // 
	private String member_phone; // 
	private String member_mail; // 
	private String member_date; // 

	private String member_type_name; // 
	private String ids;
	private String random;
	
	public String getMember_sexDesc(){
		switch (member_sex) {
		case 1:
			return "��";
		case 2:
			return "Ů";
		default:
			return "��";
		}
	}
	
	public int getMember_id() {
		return member_id;
	}
	public String getMember_no() {
		return member_no;
	}
	public String getMember_name() {
		return member_name;
	}
	public int getMember_type_id() {
		return member_type_id;
	}
	public String getMember_card() {
		return member_card;
	}
	public int getMember_sex() {
		return member_sex;
	}
	public int getMember_age() {
		return member_age;
	}
	public String getMember_phone() {
		return member_phone;
	}
	public String getMember_mail() {
		return member_mail;
	}
	public String getMember_date() {
		return member_date;
	}
	public String getIds() {
		return ids;
	}
	public String getRandom() {
		return random;
	}
	public void setMember_id(int member_id) {
		this.member_id = member_id;
	}
	public void setMember_no(String member_no) {
		this.member_no = member_no;
	}
	public void setMember_name(String member_name) {
		this.member_name = member_name;
	}
	public void setMember_type_id(int member_type_id) {
		this.member_type_id = member_type_id;
	}
	public void setMember_card(String member_card) {
		this.member_card = member_card;
	}
	public void setMember_sex(int member_sex) {
		this.member_sex = member_sex;
	}
	public void setMember_age(int member_age) {
		this.member_age = member_age;
	}
	public void setMember_phone(String member_phone) {
		this.member_phone = member_phone;
	}
	public void setMember_mail(String member_mail) {
		this.member_mail = member_mail;
	}
	public void setMember_date(String member_date) {
		this.member_date = member_date;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public void setRandom(String random) {
		this.random = random;
	}

	public String getMember_type_name() {
		return member_type_name;
	}

	public void setMember_type_name(String member_type_name) {
		this.member_type_name = member_type_name;
	}

	 
}
