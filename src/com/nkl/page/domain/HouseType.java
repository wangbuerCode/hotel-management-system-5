package com.nkl.page.domain;

import com.nkl.common.domain.BaseDomain;

public class HouseType extends BaseDomain {
	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = 1770185824735782580L;
	private int house_type_id; // 
	private String house_type_no; // 
	private String house_type_name; // 
	private double house_price; // 
	private String note; // 

	private String ids;

	public int getHouse_type_id() {
		return house_type_id;
	}

	public String getHouse_type_no() {
		return house_type_no;
	}

	public String getHouse_type_name() {
		return house_type_name;
	}

	public double getHouse_price() {
		return house_price;
	}

	public String getNote() {
		return note;
	}

	public String getIds() {
		return ids;
	}

	public void setHouse_type_id(int house_type_id) {
		this.house_type_id = house_type_id;
	}

	public void setHouse_type_no(String house_type_no) {
		this.house_type_no = house_type_no;
	}

	public void setHouse_type_name(String house_type_name) {
		this.house_type_name = house_type_name;
	}

	public void setHouse_price(double house_price) {
		this.house_price = house_price;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

}
