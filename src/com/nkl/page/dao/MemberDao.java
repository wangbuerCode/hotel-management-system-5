package com.nkl.page.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.nkl.common.dao.BaseDao;
import com.nkl.common.util.StringUtil;
import com.nkl.page.domain.Member;

public class MemberDao {

	public int addMember(Member member, Connection conn){
		String sql = "INSERT INTO member(member_id,member_no,member_name,member_type_id,member_card,member_sex,member_age,member_phone,member_mail,member_date) values(null,?,?,?,?,?,?,?,?,?)";
		Object[] params = new Object[] {
			member.getMember_no(),
			member.getMember_name(),
			member.getMember_type_id(),
			member.getMember_card(),
			member.getMember_sex(),
			member.getMember_age(),
			member.getMember_phone(),
			member.getMember_mail(),
			member.getMember_date()
		};
		return BaseDao.executeUpdate(sql, params, conn );
	}

	public int delMember(String member_id, Connection conn){
		String sql = "DELETE FROM member WHERE member_id=?";

		Object[] params = new Object[] { new Integer(member_id)};
		return BaseDao.executeUpdate(sql, params, conn);
	}

	public int delMembers(String[] member_ids, Connection conn){
		StringBuilder sBuilder = new StringBuilder();
		for (int i = 0; i <member_ids.length; i++) {
			sBuilder.append("?");
			if (i !=member_ids.length-1) {
				sBuilder.append(",");
			}
		}
		String sql = "DELETE FROM member WHERE member_id IN(" +sBuilder.toString()+")";

		Object[] params = member_ids;

		return BaseDao.executeUpdate(sql, params, conn);
	}

	public int updateMember(Member member, Connection conn){
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("UPDATE member SET member_id = " + member.getMember_id() +" ");
		if (!StringUtil.isEmptyString(member.getMember_name())) {
			sBuilder.append(",member_name = '" + member.getMember_name() +"' ");
		}
		if (member.getMember_type_id()!=0) {
			sBuilder.append(",member_type_id = " + member.getMember_type_id() +" ");
		}
		if (!StringUtil.isEmptyString(member.getMember_card())) {
			sBuilder.append(",member_card = '" + member.getMember_card() +"' ");
		}
		if (member.getMember_sex()!=0) {
			sBuilder.append(",member_sex = " + member.getMember_sex() +" ");
		}
		if (member.getMember_age()!=0) {
			sBuilder.append(",member_age = " + member.getMember_age() +" ");
		}
		if (!StringUtil.isEmptyString(member.getMember_phone())) {
			sBuilder.append(",member_phone = '" + member.getMember_phone() +"' ");
		}
		if (!StringUtil.isEmptyString(member.getMember_mail())) {
			sBuilder.append(",member_mail = '" + member.getMember_mail() +"' ");
		}
		
		sBuilder.append("where member_id = " + member.getMember_id() +" ");

		Object[] params = null;
		return BaseDao.executeUpdate(sBuilder.toString(), params, conn);
	}

	public Member getMember(Member member, Connection conn){
		Member _member=null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT m.*,t.member_type_name FROM member m join member_type t on m.member_type_id=t.member_type_id WHERE 1=1");
		if (member.getMember_id()!=0) {
			sBuilder.append(" and member_id = " + member.getMember_id() +" ");
		}
		if (!StringUtil.isEmptyString(member.getMember_no())) {
			sBuilder.append(" and member_no = '" + member.getMember_no() +"' ");
		}

		List<Object> list = BaseDao.executeQuery(Member.class.getName(), sBuilder.toString(), null, conn);
		if (list != null && list.size() > 0) {
			 _member = (Member)list.get(0);
		}
		return _member;
	}

	public List<Member>  listMembers(Member member, Connection conn){
		List<Member> members = null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT * FROM (");
		sBuilder.append("SELECT m.*,t.member_type_name FROM member m join member_type t on m.member_type_id=t.member_type_id WHERE 1=1");

		if (member.getMember_id()!=0) {
			sBuilder.append(" and member_id = " + member.getMember_id() +" ");
		}
		if (!StringUtil.isEmptyString(member.getMember_no())) {
			sBuilder.append(" and member_no like '%" + member.getMember_no() +"%' ");
		}
		if (!StringUtil.isEmptyString(member.getMember_name())) {
			sBuilder.append(" and member_name like '%" + member.getMember_name() +"%' ");
		}
		if (member.getMember_type_id()!=0) {
			sBuilder.append(" and m.member_type_id = " + member.getMember_type_id() +" ");
		}
		if (!StringUtil.isEmptyString(member.getMember_type_name())) {
			sBuilder.append(" and t.member_type_name like '%" + member.getMember_type_name() +"%' ");
		}
		sBuilder.append(" order by member_id asc) t");

		if (member.getStart() != -1) {
			sBuilder.append(" limit " + member.getStart() + "," + member.getLimit());
		}

		List<Object> list = BaseDao.executeQuery(Member.class.getName(), sBuilder.toString(), null, conn);
		if (list != null && list.size() > 0) {
			members = new ArrayList<Member>();
			for (Object object : list) {
				members.add((Member)object);
			}
		}
		return members;
	}

	public int  listMembersCount(Member member, Connection conn){
		int sum = 0;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT count(*) FROM member m join member_type t on m.member_type_id=t.member_type_id WHERE 1=1");

		if (member.getMember_id()!=0) {
			sBuilder.append(" and member_id = " + member.getMember_id() +" ");
		}
		if (!StringUtil.isEmptyString(member.getMember_no())) {
			sBuilder.append(" and member_no like '%" + member.getMember_no() +"%' ");
		}
		if (!StringUtil.isEmptyString(member.getMember_name())) {
			sBuilder.append(" and member_name like '%" + member.getMember_name() +"%' ");
		}
		if (member.getMember_type_id()!=0) {
			sBuilder.append(" and m.member_type_id = " + member.getMember_type_id() +" ");
		}
		if (!StringUtil.isEmptyString(member.getMember_type_name())) {
			sBuilder.append(" and t.member_type_name like '%" + member.getMember_type_name() +"%' ");
		}

		long count = (Long)BaseDao.executeQueryObject(sBuilder.toString(), null, conn);
		sum = (int)count;
		return sum;
	}

}
