package com.nkl.page.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.nkl.page.domain.MemberType;
import com.nkl.common.dao.BaseDao;
import com.nkl.common.util.StringUtil;

public class MemberTypeDao {

	public int addMemberType(MemberType memberType, Connection conn){
		String sql = "INSERT INTO member_type(member_type_id,member_type_name) values(null,?)";
		Object[] params = new Object[] {
			memberType.getMember_type_name()
		};
		return BaseDao.executeUpdate(sql, params, conn );
	}

	public int delMemberType(String member_type_id, Connection conn){
		String sql = "DELETE FROM member_type WHERE member_type_id=?";

		Object[] params = new Object[] { new Integer(member_type_id)};
		return BaseDao.executeUpdate(sql, params, conn);
	}

	public int delMemberTypes(String[] member_type_ids, Connection conn){
		StringBuilder sBuilder = new StringBuilder();
		for (int i = 0; i <member_type_ids.length; i++) {
			sBuilder.append("?");
			if (i !=member_type_ids.length-1) {
				sBuilder.append(",");
			}
		}
		String sql = "DELETE FROM member_type WHERE member_type_id IN(" +sBuilder.toString()+")";

		Object[] params = member_type_ids;

		return BaseDao.executeUpdate(sql, params, conn);
	}

	public int updateMemberType(MemberType memberType, Connection conn){
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("UPDATE member_type SET member_type_id = " + memberType.getMember_type_id() +" ");
		if (!StringUtil.isEmptyString(memberType.getMember_type_name())) {
			sBuilder.append(" ,member_type_name = '" + memberType.getMember_type_name() +"' ");
		}
		sBuilder.append(" where member_type_id = " + memberType.getMember_type_id() +" ");

		Object[] params = null;
		return BaseDao.executeUpdate(sBuilder.toString(), params, conn);
	}

	public MemberType getMemberType(MemberType memberType, Connection conn){
		MemberType _memberType=null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT * FROM member_type WHERE 1=1");
		if (memberType.getMember_type_id()!=0) {
			sBuilder.append(" and member_type_id = " + memberType.getMember_type_id() +" ");
		}
		if (!StringUtil.isEmptyString(memberType.getMember_type_name())) {
			sBuilder.append(" and member_type_name = '" + memberType.getMember_type_name() +"' ");
		}

		List<Object> list = BaseDao.executeQuery(MemberType.class.getName(), sBuilder.toString(), null, conn);
		if (list != null && list.size() > 0) {
			 _memberType = (MemberType)list.get(0);
		}
		return _memberType;
	}

	public List<MemberType>  listMemberTypes(MemberType memberType, Connection conn){
		List<MemberType> memberTypes = null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT * FROM (");
		sBuilder.append("SELECT * FROM member_type WHERE 1=1");

		if (memberType.getMember_type_id()!=0) {
			sBuilder.append(" and member_type_id = " + memberType.getMember_type_id() +" ");
		}
		if (!StringUtil.isEmptyString(memberType.getMember_type_name())) {
			sBuilder.append(" and member_type_name like '%" + memberType.getMember_type_name() +"%' ");
		}
		sBuilder.append(" order by member_type_id asc) t");

		if (memberType.getStart() != -1) {
			sBuilder.append(" limit " + memberType.getStart() + "," + memberType.getLimit());
		}

		List<Object> list = BaseDao.executeQuery(MemberType.class.getName(), sBuilder.toString(), null, conn);
		if (list != null && list.size() > 0) {
			memberTypes = new ArrayList<MemberType>();
			for (Object object : list) {
				memberTypes.add((MemberType)object);
			}
		}
		return memberTypes;
	}

	public int  listMemberTypesCount(MemberType memberType, Connection conn){
		int sum = 0;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT count(*) FROM member_type WHERE 1=1");

		if (memberType.getMember_type_id()!=0) {
			sBuilder.append(" and member_type_id = " + memberType.getMember_type_id() +" ");
		}
		if (!StringUtil.isEmptyString(memberType.getMember_type_name())) {
			sBuilder.append(" and member_type_name like '%" + memberType.getMember_type_name() +"%' ");
		}

		long count = (Long)BaseDao.executeQueryObject(sBuilder.toString(), null, conn);
		sum = (int)count;
		return sum;
	}

}
