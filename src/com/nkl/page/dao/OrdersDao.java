package com.nkl.page.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.nkl.page.domain.Orders;
import com.nkl.common.dao.BaseDao;
import com.nkl.common.util.StringUtil;

public class OrdersDao {

	public int addOrders(Orders orders, Connection conn){
		String sql = "INSERT INTO orders(orders_id,member_id,member_name,member_phone,member_card,house_id,stay_date,stay_days,orders_flag,orders_date) values(null,?,?,?,?,?,?,?,?,?)";
		Object[] params = new Object[] {
			orders.getMember_id(),
			orders.getMember_name(),
			orders.getMember_phone(),
			orders.getMember_card(),
			orders.getHouse_id(),
			orders.getStay_date(),
			orders.getStay_days(),
			orders.getOrders_flag(),
			orders.getOrders_date()
		};
		return BaseDao.executeUpdate(sql, params, conn );
	}

	public int delOrders(String orders_id, Connection conn){
		String sql = "DELETE FROM orders WHERE orders_id=?";

		Object[] params = new Object[] { new Integer(orders_id)};
		return BaseDao.executeUpdate(sql, params, conn);
	}

	public int delOrderss(String[] orders_ids, Connection conn){
		StringBuilder sBuilder = new StringBuilder();
		for (int i = 0; i <orders_ids.length; i++) {
			sBuilder.append("?");
			if (i !=orders_ids.length-1) {
				sBuilder.append(",");
			}
		}
		String sql = "DELETE FROM orders WHERE orders_id IN(" +sBuilder.toString()+")";

		Object[] params = orders_ids;

		return BaseDao.executeUpdate(sql, params, conn);
	}

	public int updateOrders(Orders orders, Connection conn){
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("UPDATE orders SET orders_id = " + orders.getOrders_id() +" ");
		if (orders.getOrders_flag()!=0) {
			sBuilder.append(" ,orders_flag = " + orders.getOrders_flag() +" ");
		}
		sBuilder.append(" where orders_id = " + orders.getOrders_id() +" ");

		Object[] params = null;
		return BaseDao.executeUpdate(sBuilder.toString(), params, conn);
	}
	
	public Orders getOrders(Orders orders, Connection conn){
		Orders _orders=null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT o.*,g.house_no,g.house_type_id,gt.house_type_name,gt.house_price From orders o ");
		sBuilder.append("  join house g on g.house_id=o.house_id  ");
		sBuilder.append("  join house_type gt on g.house_type_id=gt.house_type_id WHERE 1=1");
		if (orders.getOrders_id()!=0) {
			sBuilder.append(" and orders_id = " + orders.getOrders_id() +" ");
		}

		List<Object> list = BaseDao.executeQuery(Orders.class.getName(), sBuilder.toString(), null, conn);
		if (list != null && list.size() > 0) {
			 _orders = (Orders)list.get(0);
		}
		return _orders;
	}

	public List<Orders>  listOrderss(Orders orders, Connection conn){
		List<Orders> orderss = null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT * FROM (");
		sBuilder.append("SELECT o.*,g.house_no,g.house_type_id,gt.house_type_name,gt.house_price From orders o ");
		sBuilder.append("  join house g on g.house_id=o.house_id  ");
		sBuilder.append("  join house_type gt on g.house_type_id=gt.house_type_id WHERE 1=1");

		if (orders.getOrders_id()!=0) {
			sBuilder.append(" and orders_id = " + orders.getOrders_id() +" ");
		}
		if (orders.getHouse_id()!=0) {
			sBuilder.append(" and o.house_id = " + orders.getHouse_id() +" ");
		}
		if (!StringUtil.isEmptyString(orders.getHouse_no())) {
			sBuilder.append(" and g.house_no like '%" + orders.getHouse_no() +"%' ");
		}
		if (orders.getHouse_type_id()!=0) {
			sBuilder.append(" and gt.house_type_id = " + orders.getHouse_type_id() +" ");
		}
		if (!StringUtil.isEmptyString(orders.getHouse_type_name())) {
			sBuilder.append(" and gt.house_type_name like '%" + orders.getHouse_type_name() +"%' ");
		}
		if (!StringUtil.isEmptyString(orders.getOrders_date_min())) {
			sBuilder.append(" and o.orders_date >= str_to_date('" + orders.getOrders_date_min() +"','%Y-%m-%d') ");
		}
		if (!StringUtil.isEmptyString(orders.getOrders_date_max())) {
			sBuilder.append(" and o.orders_date <= str_to_date('" + orders.getOrders_date_max() +"','%Y-%m-%d') ");
		}
		if (orders.getMember_id()!=0) {
			sBuilder.append(" and o.member_id = " + orders.getMember_id() +" ");
		}
		if (!StringUtil.isEmptyString(orders.getMember_name())) {
			sBuilder.append(" and o.member_name like '%" + orders.getMember_name() +"%' ");
		}
		if (orders.getOrders_flag()!=0) {
			sBuilder.append(" and o.orders_flag = " + orders.getOrders_flag() +" ");
		}
		if (!StringUtil.isEmptyString(orders.getOrders_flags())) {
			sBuilder.append(" and o.orders_flag in (" + orders.getOrders_flags() +") ");
		}
		sBuilder.append(" order by orders_date desc,orders_id asc) t");

		if (orders.getStart() != -1) {
			sBuilder.append(" limit " + orders.getStart() + "," + orders.getLimit());
		}

		List<Object> list = BaseDao.executeQuery(Orders.class.getName(), sBuilder.toString(), null, conn);
		if (list != null && list.size() > 0) {
			orderss = new ArrayList<Orders>();
			for (Object object : list) {
				orderss.add((Orders)object);
			}
		}
		return orderss;
	}

	public int  listOrderssCount(Orders orders, Connection conn){
		int sum = 0;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT count(*) From orders o ");
		sBuilder.append("  join house g on g.house_id=o.house_id  ");
		sBuilder.append("  join house_type gt on g.house_type_id=gt.house_type_id WHERE 1=1");

		if (orders.getOrders_id()!=0) {
			sBuilder.append(" and orders_id = " + orders.getOrders_id() +" ");
		}
		if (orders.getHouse_id()!=0) {
			sBuilder.append(" and o.house_id = " + orders.getHouse_id() +" ");
		}
		if (!StringUtil.isEmptyString(orders.getHouse_no())) {
			sBuilder.append(" and g.house_no like '%" + orders.getHouse_no() +"%' ");
		}
		if (orders.getHouse_type_id()!=0) {
			sBuilder.append(" and gt.house_type_id = " + orders.getHouse_type_id() +" ");
		}
		if (!StringUtil.isEmptyString(orders.getHouse_type_name())) {
			sBuilder.append(" and gt.house_type_name like '%" + orders.getHouse_type_name() +"%' ");
		}
		if (!StringUtil.isEmptyString(orders.getOrders_date_min())) {
			sBuilder.append(" and o.orders_date >= str_to_date('" + orders.getOrders_date_min() +"','%Y-%m-%d') ");
		}
		if (!StringUtil.isEmptyString(orders.getOrders_date_max())) {
			sBuilder.append(" and o.orders_date <= str_to_date('" + orders.getOrders_date_max() +"','%Y-%m-%d') ");
		}
		if (orders.getMember_id()!=0) {
			sBuilder.append(" and o.member_id = " + orders.getMember_id() +" ");
		}
		if (!StringUtil.isEmptyString(orders.getMember_name())) {
			sBuilder.append(" and o.member_name like '%" + orders.getMember_name() +"%' ");
		}
		if (orders.getOrders_flag()!=0) {
			sBuilder.append(" and o.orders_flag = " + orders.getOrders_flag() +" ");
		}
		if (!StringUtil.isEmptyString(orders.getOrders_flags())) {
			sBuilder.append(" and o.orders_flag in (" + orders.getOrders_flags() +") ");
		}

		long count = (Long)BaseDao.executeQueryObject(sBuilder.toString(), null, conn);
		sum = (int)count;
		return sum;
	}

}
