package com.nkl.page.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.nkl.common.dao.BaseDao;
import com.nkl.common.util.StringUtil;
import com.nkl.page.domain.Stay;

public class StayDao {

	public int addStay(Stay stay, Connection conn){
		String sql = "INSERT INTO stay(stay_id,house_id,member_id,member_name,member_card,member_phone,stay_date,leave_date,stay_money,other_money,all_money,stay_flag) values(null,?,?,?,?,?,?,?,?,?,?,?)";
		Object[] params = new Object[] {
			stay.getHouse_id(),
			stay.getMember_id(),
			stay.getMember_name(),
			stay.getMember_card(),
			stay.getMember_phone(),
			stay.getStay_date(),
			stay.getLeave_date(),
			stay.getStay_money(),
			stay.getOther_money(),
			stay.getAll_money(),
			stay.getStay_flag()

		};
		return BaseDao.executeUpdate(sql, params, conn );
	}

	public int delStay(String stay_id, Connection conn){
		String sql = "DELETE FROM stay WHERE stay_id=?";

		Object[] params = new Object[] { new Integer(stay_id)};
		return BaseDao.executeUpdate(sql, params, conn);
	}

	public int delStays(String[] stay_ids, Connection conn){
		StringBuilder sBuilder = new StringBuilder();
		for (int i = 0; i <stay_ids.length; i++) {
			sBuilder.append("?");
			if (i !=stay_ids.length-1) {
				sBuilder.append(",");
			}
		}
		String sql = "DELETE FROM stay WHERE stay_id IN(" +sBuilder.toString()+")";

		Object[] params = stay_ids;

		return BaseDao.executeUpdate(sql, params, conn);
	}

	public int updateStay(Stay stay, Connection conn){
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("UPDATE stay SET stay_id = " + stay.getStay_id() +" ");
		if (!StringUtil.isEmptyString(stay.getLeave_date())) {
			sBuilder.append(", leave_date = '" + stay.getLeave_date() +"' ");
		}
		if (stay.getOther_money()!=0) {
			sBuilder.append(", other_money = " + stay.getOther_money() +" ");
		}
		if (stay.getAll_money()!=0) {
			sBuilder.append(", all_money = " + stay.getAll_money() +" ");
		}
		if (stay.getStay_flag()!=0) {
			sBuilder.append(" ,stay_flag = " + stay.getStay_flag() +" ");
		}
		sBuilder.append("where stay_id = " + stay.getStay_id() +" ");

		Object[] params = null;
		return BaseDao.executeUpdate(sBuilder.toString(), params, conn);
	}

	public Stay getStay(Stay stay, Connection conn){
		Stay _stay=null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT o.*,g.house_no,g.house_type_id,gt.house_type_name,gt.house_price From stay o ");
		sBuilder.append("  join house g on g.house_id=o.house_id  ");
		sBuilder.append("  join house_type gt on g.house_type_id=gt.house_type_id WHERE 1=1");
		if (stay.getStay_id()!=0) {
			sBuilder.append(" and stay_id = " + stay.getStay_id() +" ");
		}

		List<Object> list = BaseDao.executeQuery(Stay.class.getName(), sBuilder.toString(), null, conn);
		if (list != null && list.size() > 0) {
			 _stay = (Stay)list.get(0);
		}
		return _stay;
	}

	public List<Stay>  listStays(Stay stay, Connection conn){
		List<Stay> stays = null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT * FROM (");
		sBuilder.append("SELECT o.*,g.house_no,g.house_type_id,gt.house_type_name,gt.house_price From stay o ");
		sBuilder.append("  join house g on g.house_id=o.house_id  ");
		sBuilder.append("  join house_type gt on g.house_type_id=gt.house_type_id WHERE 1=1");

		if (stay.getHouse_id()!=0) {
			sBuilder.append(" and o.house_id = " + stay.getHouse_id() +" ");
		}
		if (!StringUtil.isEmptyString(stay.getHouse_no())) {
			sBuilder.append(" and g.house_no like '%" + stay.getHouse_no() +"%' ");
		}
		if (stay.getHouse_type_id()!=0) {
			sBuilder.append(" and gt.house_type_id = " + stay.getHouse_type_id() +" ");
		}
		if (!StringUtil.isEmptyString(stay.getHouse_type_name())) {
			sBuilder.append(" and gt.house_type_name like '%" + stay.getHouse_type_name() +"%' ");
		}
		if (!StringUtil.isEmptyString(stay.getStay_date_min())) {
			sBuilder.append(" and o.stay_date >= str_to_date('" + stay.getStay_date_min() +"','%Y-%m-%d') ");
		}
		if (!StringUtil.isEmptyString(stay.getStay_date_max())) {
			sBuilder.append(" and o.stay_date <= str_to_date('" + stay.getStay_date_max() +"','%Y-%m-%d') ");
		}
		if (stay.getMember_id()!=0) {
			sBuilder.append(" and o.member_id = " + stay.getMember_id() +" ");
		}
		if (!StringUtil.isEmptyString(stay.getMember_name())) {
			sBuilder.append(" and o.member_name like '%" + stay.getMember_name() +"%' ");
		}
		if (stay.getStay_flag()!=0) {
			sBuilder.append(" and o.stay_flag = " + stay.getStay_flag() +" ");
		}
		sBuilder.append(" order by stay_date desc,stay_id asc) t");

		if (stay.getStart() != -1) {
			sBuilder.append(" limit " + stay.getStart() + "," + stay.getLimit());
		}

		List<Object> list = BaseDao.executeQuery(Stay.class.getName(), sBuilder.toString(), null, conn);
		if (list != null && list.size() > 0) {
			stays = new ArrayList<Stay>();
			for (Object object : list) {
				stays.add((Stay)object);
			}
		}
		return stays;
	}

	public int  listStaysCount(Stay stay, Connection conn){
		int sum = 0;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT count(*) From stay o ");
		sBuilder.append("  join house g on g.house_id=o.house_id  ");
		sBuilder.append("  join house_type gt on g.house_type_id=gt.house_type_id WHERE 1=1");

		if (stay.getHouse_id()!=0) {
			sBuilder.append(" and o.house_id = " + stay.getHouse_id() +" ");
		}
		if (!StringUtil.isEmptyString(stay.getHouse_no())) {
			sBuilder.append(" and g.house_no like '%" + stay.getHouse_no() +"%' ");
		}
		if (stay.getHouse_type_id()!=0) {
			sBuilder.append(" and gt.house_type_id = " + stay.getHouse_type_id() +" ");
		}
		if (!StringUtil.isEmptyString(stay.getHouse_type_name())) {
			sBuilder.append(" and gt.house_type_name like '%" + stay.getHouse_type_name() +"%' ");
		}
		if (!StringUtil.isEmptyString(stay.getStay_date_min())) {
			sBuilder.append(" and o.stay_date >= str_to_date('" + stay.getStay_date_min() +"','%Y-%m-%d') ");
		}
		if (!StringUtil.isEmptyString(stay.getStay_date_max())) {
			sBuilder.append(" and o.stay_date <= str_to_date('" + stay.getStay_date_max() +"','%Y-%m-%d') ");
		}
		if (stay.getMember_id()!=0) {
			sBuilder.append(" and o.member_id = " + stay.getMember_id() +" ");
		}
		if (!StringUtil.isEmptyString(stay.getMember_name())) {
			sBuilder.append(" and o.member_name like '%" + stay.getMember_name() +"%' ");
		}
		if (stay.getStay_flag()!=0) {
			sBuilder.append(" and o.stay_flag = " + stay.getStay_flag() +" ");
		}

		long count = (Long)BaseDao.executeQueryObject(sBuilder.toString(), null, conn);
		sum = (int)count;
		return sum;
	}

}
