package com.nkl.page.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.nkl.common.dao.BaseDao;
import com.nkl.common.util.StringUtil;
import com.nkl.page.domain.House;

public class HouseDao {

	public int addHouse(House house, Connection conn){
		String sql = "INSERT INTO house(house_id,house_no,house_type_id,house_desc,house_flag) values(null,?,?,?,?)";
		Object[] params = new Object[] {
			house.getHouse_no(),
			house.getHouse_type_id(),
			house.getHouse_desc(),
			house.getHouse_flag()

		};
		return BaseDao.executeUpdate(sql, params, conn );
	}

	public int delHouse(String house_id, Connection conn){
		String sql = "DELETE FROM house WHERE house_id=?";

		Object[] params = new Object[] { new Integer(house_id)};
		return BaseDao.executeUpdate(sql, params, conn);
	}

	public int delHouses(String[] house_ids, Connection conn){
		StringBuilder sBuilder = new StringBuilder();
		for (int i = 0; i <house_ids.length; i++) {
			sBuilder.append("?");
			if (i !=house_ids.length-1) {
				sBuilder.append(",");
			}
		}
		String sql = "DELETE FROM house WHERE house_id IN(" +sBuilder.toString()+")";

		Object[] params = house_ids;

		return BaseDao.executeUpdate(sql, params, conn);
	}

	public int updateHouse(House house, Connection conn){
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("UPDATE house SET house_id = " + house.getHouse_id() +" ");
		if (house.getHouse_type_id()!=0) {
			sBuilder.append(" ,house_type_id = " + house.getHouse_type_id() +" ");
		}
		if (!StringUtil.isEmptyString(house.getHouse_no())) {
			sBuilder.append(" ,house_no = '" + house.getHouse_no() +"' ");
		}
		if (house.getHouse_flag()!=0) {
			sBuilder.append(" ,house_flag = " + house.getHouse_flag() +" ");
		}
		if (!StringUtil.isEmptyString(house.getHouse_desc())) {
			sBuilder.append(" ,house_desc = '" + house.getHouse_desc() +"' ");
		}
		
		sBuilder.append(" where house_id = " + house.getHouse_id() +" ");

		Object[] params = null;
		return BaseDao.executeUpdate(sBuilder.toString(), params, conn);
	}
	
	public int updateHouseByDelOrdersIds(String[] orders_ids, Connection conn){
		StringBuilder sBuilder = new StringBuilder();
		for (int i = 0; i <orders_ids.length; i++) {
			sBuilder.append("?");
			if (i !=orders_ids.length-1) {
				sBuilder.append(",");
			}
		}
		String sql = "Update house h set h.house_flag=1 where h.house_id in (select house_id from orders WHERE orders_id IN(" +sBuilder.toString()+") and orders_flag=2)";

		Object[] params = orders_ids;

		return BaseDao.executeUpdate(sql, params, conn);
	}
	
	public int updateHouseByDelStayIds(String[] orders_ids, Connection conn){
		StringBuilder sBuilder = new StringBuilder();
		for (int i = 0; i <orders_ids.length; i++) {
			sBuilder.append("?");
			if (i !=orders_ids.length-1) {
				sBuilder.append(",");
			}
		}
		String sql = "Update house h set h.house_flag=1 where h.house_id in (select house_id from stay WHERE stay_id IN(" +sBuilder.toString()+") and stay_flag=1)";

		Object[] params = orders_ids;

		return BaseDao.executeUpdate(sql, params, conn);
	}

	public House getHouse(House house, Connection conn){
		House _house=null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT b.*,bt.house_type_name,bt.house_price FROM house b left join house_type bt on b.house_type_id=bt.house_type_id WHERE 1=1");
		if (house.getHouse_id()!=0) {
			sBuilder.append(" and house_id = " + house.getHouse_id() +" ");
		}
		if (!StringUtil.isEmptyString(house.getHouse_no())) {
			sBuilder.append(" and house_no = '" + house.getHouse_no() +"' ");
		}

		List<Object> list = BaseDao.executeQuery(House.class.getName(), sBuilder.toString(), null, conn);
		if (list != null && list.size() > 0) {
			 _house = (House)list.get(0);
		}
		return _house;
	}

	public List<House>  listHouses(House house, Connection conn){
		List<House> houses = null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT * FROM (");
		sBuilder.append("SELECT b.*,bt.house_type_name,bt.house_price FROM house b left join house_type bt on b.house_type_id=bt.house_type_id WHERE 1=1");

		if (house.getHouse_id()!=0) {
			sBuilder.append(" and house_id = " + house.getHouse_id() +" ");
		}
		if (!StringUtil.isEmptyString(house.getHouse_no())) {
			sBuilder.append(" and house_no like '%" + house.getHouse_no() +"%' ");
		}
		if (house.getHouse_type_id()!=0) {
			sBuilder.append(" and b.house_type_id = " + house.getHouse_type_id() +" ");
		}
		if (house.getHouse_flag()!=0) {
			sBuilder.append(" and b.house_flag = " + house.getHouse_flag() +" ");
		}
		
		sBuilder.append(" order by house_id asc) t");

		if (house.getStart() != -1) {
			sBuilder.append(" limit " + house.getStart() + "," + house.getLimit());
		}

		List<Object> list = BaseDao.executeQuery(House.class.getName(), sBuilder.toString(), null, conn);
		if (list != null && list.size() > 0) {
			houses = new ArrayList<House>();
			for (Object object : list) {
				houses.add((House)object);
			}
		}
		return houses;
	}

	public int  listHousesCount(House house, Connection conn){
		int sum = 0;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT count(*) FROM house b left join house_type bt on b.house_type_id=bt.house_type_id WHERE 1=1");

		if (house.getHouse_id()!=0) {
			sBuilder.append(" and house_id = " + house.getHouse_id() +" ");
		}
		if (!StringUtil.isEmptyString(house.getHouse_no())) {
			sBuilder.append(" and house_no like '%" + house.getHouse_no() +"%' ");
		}
		if (house.getHouse_type_id()!=0) {
			sBuilder.append(" and b.house_type_id = " + house.getHouse_type_id() +" ");
		}
		if (house.getHouse_flag()!=0) {
			sBuilder.append(" and b.house_flag = " + house.getHouse_flag() +" ");
		}

		long count = (Long)BaseDao.executeQueryObject(sBuilder.toString(), null, conn);
		sum = (int)count;
		return sum;
	}
	
}
