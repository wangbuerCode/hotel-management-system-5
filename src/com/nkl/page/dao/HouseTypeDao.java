package com.nkl.page.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.nkl.page.domain.HouseType;
import com.nkl.common.dao.BaseDao;
import com.nkl.common.util.StringUtil;

public class HouseTypeDao {

	public int addHouseType(HouseType houseType, Connection conn){
		String sql = "INSERT INTO house_type(house_type_id,house_type_no,house_type_name,house_price,note) values(null,?,?,?,?)";
		Object[] params = new Object[] {
			houseType.getHouse_type_no(),
			houseType.getHouse_type_name(),
			houseType.getHouse_price(),
			houseType.getNote()

		};
		return BaseDao.executeUpdate(sql, params, conn );
	}

	public int delHouseType(String house_type_id, Connection conn){
		String sql = "DELETE FROM house_type WHERE house_type_id=?";

		Object[] params = new Object[] { new Integer(house_type_id)};
		return BaseDao.executeUpdate(sql, params, conn);
	}

	public int delHouseTypes(String[] house_type_ids, Connection conn){
		StringBuilder sBuilder = new StringBuilder();
		for (int i = 0; i <house_type_ids.length; i++) {
			sBuilder.append("?");
			if (i !=house_type_ids.length-1) {
				sBuilder.append(",");
			}
		}
		String sql = "DELETE FROM house_type WHERE house_type_id IN(" +sBuilder.toString()+")";

		Object[] params = house_type_ids;

		return BaseDao.executeUpdate(sql, params, conn);
	}

	public int updateHouseType(HouseType houseType, Connection conn){
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("UPDATE house_type SET house_type_id = " + houseType.getHouse_type_id() +" ");
		if (!StringUtil.isEmptyString(houseType.getHouse_type_no())) {
			sBuilder.append(" ,house_type_no = '" + houseType.getHouse_type_no() +"' ");
		}
		if (!StringUtil.isEmptyString(houseType.getHouse_type_name())) {
			sBuilder.append(" ,house_type_name = '" + houseType.getHouse_type_name() +"' ");
		}
		if (houseType.getHouse_price()!=0) {
			sBuilder.append(" ,house_price = " + houseType.getHouse_price() +" ");
		}
		sBuilder.append(" where house_type_id = " + houseType.getHouse_type_id() +" ");

		Object[] params = null;
		return BaseDao.executeUpdate(sBuilder.toString(), params, conn);
	}

	public HouseType getHouseType(HouseType houseType, Connection conn){
		HouseType _houseType=null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT * FROM house_type WHERE 1=1");
		if (houseType.getHouse_type_id()!=0) {
			sBuilder.append(" and house_type_id = " + houseType.getHouse_type_id() +" ");
		}
		if (!StringUtil.isEmptyString(houseType.getHouse_type_no())) {
			sBuilder.append(" and house_type_no = '" + houseType.getHouse_type_no() +"' ");
		}

		List<Object> list = BaseDao.executeQuery(HouseType.class.getName(), sBuilder.toString(), null, conn);
		if (list != null && list.size() > 0) {
			 _houseType = (HouseType)list.get(0);
		}
		return _houseType;
	}

	public List<HouseType>  listHouseTypes(HouseType houseType, Connection conn){
		List<HouseType> houseTypes = null;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT * FROM (");
		sBuilder.append("SELECT * FROM house_type WHERE 1=1");

		if (houseType.getHouse_type_id()!=0) {
			sBuilder.append(" and house_type_id = " + houseType.getHouse_type_id() +" ");
		}
		if (!StringUtil.isEmptyString(houseType.getHouse_type_no())) {
			sBuilder.append(" and house_type_no like '%" + houseType.getHouse_type_no() +"%' ");
		}
		if (!StringUtil.isEmptyString(houseType.getHouse_type_name())) {
			sBuilder.append(" and house_type_name like '%" + houseType.getHouse_type_name() +"%' ");
		}
		sBuilder.append(" order by house_type_id asc) t");

		if (houseType.getStart() != -1) {
			sBuilder.append(" limit " + houseType.getStart() + "," + houseType.getLimit());
		}

		List<Object> list = BaseDao.executeQuery(HouseType.class.getName(), sBuilder.toString(), null, conn);
		if (list != null && list.size() > 0) {
			houseTypes = new ArrayList<HouseType>();
			for (Object object : list) {
				houseTypes.add((HouseType)object);
			}
		}
		return houseTypes;
	}

	public int  listHouseTypesCount(HouseType houseType, Connection conn){
		int sum = 0;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("SELECT count(*) FROM house_type WHERE 1=1");

		if (houseType.getHouse_type_id()!=0) {
			sBuilder.append(" and house_type_id = " + houseType.getHouse_type_id() +" ");
		}
		if (!StringUtil.isEmptyString(houseType.getHouse_type_no())) {
			sBuilder.append(" and house_type_no like '%" + houseType.getHouse_type_no() +"%' ");
		}
		if (!StringUtil.isEmptyString(houseType.getHouse_type_name())) {
			sBuilder.append(" and house_type_name like '%" + houseType.getHouse_type_name() +"%' ");
		}

		long count = (Long)BaseDao.executeQueryObject(sBuilder.toString(), null, conn);
		sum = (int)count;
		return sum;
	}

}
