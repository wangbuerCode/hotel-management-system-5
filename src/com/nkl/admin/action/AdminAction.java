package com.nkl.admin.action;

import java.util.ArrayList;
import java.util.List;

import com.nkl.admin.manager.AdminManager;
import com.nkl.common.action.BaseAction;
import com.nkl.common.util.DateUtil;
import com.nkl.common.util.Param;
import com.nkl.page.domain.House;
import com.nkl.page.domain.HouseType;
import com.nkl.page.domain.Member;
import com.nkl.page.domain.MemberType;
import com.nkl.page.domain.Orders;
import com.nkl.page.domain.Stay;
import com.nkl.page.domain.User;

public class AdminAction  extends BaseAction {

	private static final long serialVersionUID = 1L;
	AdminManager adminManager = new AdminManager();

	//抓取页面参数
	User paramsUser;
	House paramsHouse;
	HouseType paramsHouseType;
	Member paramsMember;
	MemberType paramsMemberType;
	Orders paramsOrders;
	Stay paramsStay;
	
	String tip;
	
	/**
	 * @Title: saveAdmin
	 * @Description: 保存修改个人信息
	 * @return String
	 */
	public String saveAdmin(){
		try {
			//验证前台操作人员会话是否失效
			if (!validateAdmin()) {
				return "loginTip";
			}
			 //保存修改个人信息
			adminManager.updateUser(paramsUser);
			//更新session
			User admin = new User();
			admin.setUser_id(paramsUser.getUser_id());
			admin = adminManager.getUser(admin);
			Param.setSession("admin", admin);

			setSuccessTip("编辑成功", "modifyInfo.jsp");
		} catch (Exception e) {
			setErrorTip("编辑异常", "modifyInfo.jsp");
		}
		return "infoTip";
	}
	
	/**
	 * @Title: saveAdminPass
	 * @Description: 保存修改个人密码
	 * @return String
	 */
	public String saveAdminPass(){
		try {
			//验证前台操作人员会话是否失效
			if (!validateAdmin()) {
				return "loginTip";
			}
			 //保存修改个人密码
			adminManager.updateUser(paramsUser);
			//更新session
			User admin = (User)Param.getSession("admin");
			if (admin!=null) {
				admin.setUser_pass(paramsUser.getUser_pass());
				Param.setSession("admin", admin);
			}

			setSuccessTip("修改成功", "modifyPwd.jsp");
		} catch (Exception e) {
			setErrorTip("修改异常", "modifyPwd.jsp");
		}
		return "infoTip";
	}
	
	/**
	 * @Title: listUsers
	 * @Description: 查询前台操作人员
	 * @return String
	 */
	public String listUsers(){
		try {
			if (paramsUser==null) {
				paramsUser = new User();
			}
			//查询注册前台操作人员
			paramsUser.setUser_type(1);
			//设置分页信息
			setPagination(paramsUser);
			int[] sum={0};
			List<User> users = adminManager.listUsers(paramsUser,sum); 
			
			Param.setAttribute("users", users);
			setTotalCount(sum[0]);
			
		} catch (Exception e) {
			setErrorTip("查询前台操作人员异常", "main.jsp");
			return "infoTip";
		}
		
		return "userShow";
	}
	
	/**
	 * @Title: addUserShow
	 * @Description: 显示添加前台操作人员页面
	 * @return String
	 */
	public String addUserShow(){
		return "userEdit";
	}
	
	/**
	 * @Title: addUser
	 * @Description: 添加前台操作人员
	 * @return String
	 */
	public String addUser(){
		try {
			//判断登录账户是否重复
			User user = new User();
			user.setUser_name(paramsUser.getUser_name());
			user = adminManager.getUser(paramsUser);
			if (user!=null) {
				tip="添加失败，该登录账户已经存在";
				Param.setAttribute("user", paramsUser);
				return "userEdit";
			}
			
			 //添加前台操作人员
			paramsUser.setUser_type(1);
			paramsUser.setReg_date(DateUtil.getCurDateTime());
			adminManager.addUser(paramsUser);

			setSuccessTip("添加前台操作人员成功", "Admin_listUsers.action");
		} catch (Exception e) {
			setErrorTip("添加前台操作人员异常", "Admin_listUsers.action");
		}
		return "infoTip";
	}
	
	 
	/**
	 * @Title: editUser
	 * @Description: 编辑前台操作人员
	 * @return String
	 */
	public String editUser(){
		try {
			 //得到前台操作人员
			User user = adminManager.getUser(paramsUser);
			Param.setAttribute("user", user);
			
		} catch (Exception e) {
			setErrorTip("查询前台操作人员异常", "Admin_listUsers.action");
			return "infoTip";
		}
		
		return "userEdit";
	}
	
	/**
	 * @Title: saveUser
	 * @Description: 保存编辑前台操作人员
	 * @return String
	 */
	public String saveUser(){
		try {
			 //保存编辑前台操作人员
			adminManager.updateUser(paramsUser);
			
		} catch (Exception e) {
			tip="编辑失败";
			Param.setAttribute("user", paramsUser);
			return "userEdit";
		}
		setSuccessTip("编辑前台操作人员成功", "Admin_listUsers.action");
		return "infoTip";
	}
	
	/**
	 * @Title: delUsers
	 * @Description: 删除前台操作人员
	 * @return String
	 */
	public String delUsers(){
		try {
			 //删除前台操作人员
			adminManager.delUsers(paramsUser);

			setSuccessTip("删除前台操作人员成功", "Admin_listUsers.action");
		} catch (Exception e) {
			setErrorTip("删除前台操作人员异常", "Admin_listUsers.action");
		}
		return "infoTip";
	}
	
	/**
	 * @Title: listHouseTypes
	 * @Description: 查询客房类型
	 * @return String
	 */
	public String listHouseTypes(){
		try {
			if (paramsHouseType==null) {
				paramsHouseType = new HouseType();
			}
			
			//设置分页信息
			setPagination(paramsHouseType);
			//总的条数
			int[] sum={0};
			//查询客房类型列表
			List<HouseType> houseTypes = adminManager.listHouseTypes(paramsHouseType,sum); 
			
			Param.setAttribute("houseTypes", houseTypes);
			setTotalCount(sum[0]);
			
		} catch (Exception e) {
			setErrorTip("查询客房类型异常", "main.jsp");
			return "infoTip";
		}
		
		return "houseTypeShow";
	}
	
	/**
	 * @Title: addHouseTypeShow
	 * @Description: 显示添加客房类型页面
	 * @return String
	 */
	public String addHouseTypeShow(){
		return "houseTypeEdit";
	}
	
	/**
	 * @Title: addHouseType
	 * @Description: 添加客房类型
	 * @return String
	 */
	public String addHouseType(){
		try {
			//检查客房类型是否存在
			HouseType houseType = new HouseType();
			houseType.setHouse_type_no(paramsHouseType.getHouse_type_no());
			houseType = adminManager.queryHouseType(houseType);
			if (houseType!=null) {
				tip="失败，该类型编号已经存在！";
				Param.setAttribute("houseType", paramsHouseType);
				return "houseTypeEdit";
			}
			
			 //添加客房类型
			adminManager.addHouseType(paramsHouseType);
			
			setSuccessTip("添加成功", "Admin_listHouseTypes.action");
		} catch (Exception e) {
			setErrorTip("添加客房类型异常", "Admin_listHouseTypes.action");
		}
		
		return "infoTip";
	}
	
	 
	/**
	 * @Title: editHouseType
	 * @Description: 编辑客房类型
	 * @return String
	 */
	public String editHouseType(){
		try {
			 //得到客房类型
			HouseType houseType = adminManager.queryHouseType(paramsHouseType);
			Param.setAttribute("houseType", houseType);
			
		} catch (Exception e) {
			setErrorTip("查询客房类型异常", "Admin_listHouseTypes.action");
			return "infoTip";
		}
		
		return "houseTypeEdit";
	}
	
	/**
	 * @Title: saveHouseType
	 * @Description: 保存编辑客房类型
	 * @return String
	 */
	public String saveHouseType(){
		try {
			//检查客房类型是否存在
			HouseType houseType = new HouseType();
			houseType.setHouse_type_no(paramsHouseType.getHouse_type_no());
			houseType = adminManager.queryHouseType(houseType);
			if (houseType!=null && houseType.getHouse_type_id()!=paramsHouseType.getHouse_type_id()) {
				tip="失败，该类型编号已经存在！";
				Param.setAttribute("houseType", paramsHouseType);
				return "houseTypeEdit";
			}
			
			 //保存编辑客房类型
			adminManager.updateHouseType(paramsHouseType);
			
			setSuccessTip("编辑成功", "Admin_listHouseTypes.action");
		} catch (Exception e) {
			tip="编辑失败";
			Param.setAttribute("houseType", paramsHouseType);
			return "houseTypeEdit";
		}
		
		return "infoTip";
	}
	
	/**
	 * @Title: delHouseTypes
	 * @Description: 删除客房类型
	 * @return String
	 */
	public String delHouseTypes(){
		try {
			 //删除客房类型
			adminManager.delHouseTypes(paramsHouseType);
			
			setSuccessTip("删除客房类型成功", "Admin_listHouseTypes.action");
		} catch (Exception e) {
			setErrorTip("删除客房类型异常", "Admin_listHouseTypes.action");
		}
		
		return "infoTip";
	}
	
	
	/**
	 * @Title: listHouses
	 * @Description: 查询客房
	 * @return String
	 */
	public String listHouses(){
		try {
			if (paramsHouse==null) {
				paramsHouse = new House();
			}
			//设置分页信息
			setPagination(paramsHouse);
			int[] sum={0};
			List<House> houses = adminManager.listHouses(paramsHouse,sum); 
			
			Param.setAttribute("houses", houses);
			setTotalCount(sum[0]);
			
			//查询客房类型
			HouseType houseType = new HouseType();
			houseType.setStart(-1);
			List<HouseType> houseTypes = adminManager.listHouseTypes(houseType, null);
			Param.setAttribute("houseTypes", houseTypes);
			
		} catch (Exception e) {
			setErrorTip("查询客房异常", "main.jsp");
			return "infoTip";
		}
		
		return "houseShow";
	}
	
	/**
	 * @Title: addHouseShow
	 * @Description: 显示添加客房页面
	 * @return String
	 */
	public String addHouseShow(){
		//查询客房类型
		HouseType houseType = new HouseType();
		houseType.setStart(-1);
		List<HouseType> houseTypes = adminManager.listHouseTypes(houseType, null);
		Param.setAttribute("houseTypes", houseTypes);
		
		return "houseEdit";
	}
	
	/**
	 * @Title: addHouse
	 * @Description: 添加客房
	 * @return String
	 */
	public String addHouse(){
		try {
			//检查客房编号是否存在
			House house = new House();
			house.setHouse_no(paramsHouse.getHouse_no());
			house = adminManager.queryHouse(house);
			if (house!=null) {
				tip="失败，该客房编号已经存在！";
				Param.setAttribute("house", paramsHouse);
				return "houseEdit";
			}
			
			 //添加客房
			adminManager.addHouse(paramsHouse);

			setSuccessTip("添加客房成功", "Admin_listHouses.action");
		} catch (Exception e) {
			setErrorTip("添加客房异常", "Admin_listHouses.action");
		}
		return "infoTip";
	}
	
	/**
	 * @Title: queryHouse
	 * @Description: 查询客房详情
	 * @return String
	 */
	public String queryHouse(){
		try {
			 //得到客房
			House house = adminManager.queryHouse(paramsHouse);
			Param.setAttribute("house", house);
			
		} catch (Exception e) {
			setErrorTip("查询客房异常", "Admin_listHouses.action");
			return "infoTip";
		}
		
		return "houseDetail";
	}
	 
	/**
	 * @Title: editHouse
	 * @Description: 编辑客房
	 * @return String
	 */
	public String editHouse(){
		try {
			 //得到客房
			House house = adminManager.queryHouse(paramsHouse);
			Param.setAttribute("house", house);
			
			//查询客房类型
			HouseType houseType = new HouseType();
			houseType.setStart(-1);
			List<HouseType> houseTypes = adminManager.listHouseTypes(houseType, null);
			Param.setAttribute("houseTypes", houseTypes);
			
		} catch (Exception e) {
			setErrorTip("查询客房异常", "Admin_listHouses.action");
			return "infoTip";
		}
		
		return "houseEdit";
	}
	
	/**
	 * @Title: saveHouse
	 * @Description: 保存编辑客房
	 * @return String
	 */
	public String saveHouse(){
		try {
			 //保存编辑客房
			adminManager.updateHouse(paramsHouse);
			
		} catch (Exception e) {
			tip="编辑失败";
			Param.setAttribute("house", paramsHouse);
			
			//查询客房类型
			HouseType houseType = new HouseType();
			houseType.setStart(-1);
			List<HouseType> houseTypes = adminManager.listHouseTypes(houseType, null);
			Param.setAttribute("houseTypes", houseTypes);
			
			return "houseEdit";
		}
		setSuccessTip("编辑客房成功", "Admin_listHouses.action");
		return "infoTip";
	}
	
	/**
	 * @Title: delHouses
	 * @Description: 删除客房
	 * @return String
	 */
	public String delHouses(){
		try {
			 //删除客房
			adminManager.delHouses(paramsHouse);

			setSuccessTip("删除客房成功", "Admin_listHouses.action");
		} catch (Exception e) {
			setErrorTip("删除客房异常", "Admin_listHouses.action");
		}
		return "infoTip";
	}
	
	/**
	 * @Title: listMemberTypes
	 * @Description: 查询会员级别
	 * @return String
	 */
	public String listMemberTypes(){
		try {
			if (paramsMemberType==null) {
				paramsMemberType = new MemberType();
			}
			
			//设置分页信息
			setPagination(paramsMemberType);
			//总的条数
			int[] sum={0};
			//查询会员级别列表
			List<MemberType> memberTypes = adminManager.listMemberTypes(paramsMemberType,sum); 
			
			Param.setAttribute("memberTypes", memberTypes);
			setTotalCount(sum[0]);
			
		} catch (Exception e) {
			setErrorTip("查询会员级别异常", "main.jsp");
			return "infoTip";
		}
		
		return "memberTypeShow";
	}
	
	/**
	 * @Title: addMemberTypeShow
	 * @Description: 显示添加会员级别页面
	 * @return String
	 */
	public String addMemberTypeShow(){
		return "memberTypeEdit";
	}
	
	/**
	 * @Title: addMemberType
	 * @Description: 添加会员级别
	 * @return String
	 */
	public String addMemberType(){
		try {
			//检查会员级别是否存在
			MemberType memberType = new MemberType();
			memberType.setMember_type_name(paramsMemberType.getMember_type_name());
			memberType = adminManager.queryMemberType(memberType);
			if (memberType!=null) {
				tip="失败，该会员等级已经存在！";
				Param.setAttribute("memberType", paramsMemberType);
				return "memberTypeEdit";
			}
			
			 //添加会员级别
			adminManager.addMemberType(paramsMemberType);
			
			setSuccessTip("添加成功", "Admin_listMemberTypes.action");
		} catch (Exception e) {
			setErrorTip("添加会员级别异常", "Admin_listMemberTypes.action");
		}
		
		return "infoTip";
	}
	
	 
	/**
	 * @Title: editMemberType
	 * @Description: 编辑会员级别
	 * @return String
	 */
	public String editMemberType(){
		try {
			 //得到会员级别
			MemberType memberType = adminManager.queryMemberType(paramsMemberType);
			Param.setAttribute("memberType", memberType);
			
		} catch (Exception e) {
			setErrorTip("查询会员级别异常", "Admin_listMemberTypes.action");
			return "infoTip";
		}
		
		return "memberTypeEdit";
	}
	
	/**
	 * @Title: saveMemberType
	 * @Description: 保存编辑会员级别
	 * @return String
	 */
	public String saveMemberType(){
		try {
			//检查会员级别是否存在
			MemberType memberType = new MemberType();
			memberType.setMember_type_name(paramsMemberType.getMember_type_name());
			memberType = adminManager.queryMemberType(memberType);
			if (memberType!=null && memberType.getMember_type_id()!=paramsMemberType.getMember_type_id()) {
				tip="失败，该会员等级已经存在！";
				Param.setAttribute("memberType", paramsMemberType);
				return "memberTypeEdit";
			}
			
			 //保存编辑会员级别
			adminManager.updateMemberType(paramsMemberType);
			
			setSuccessTip("编辑成功", "Admin_listMemberTypes.action");
		} catch (Exception e) {
			tip="编辑失败";
			Param.setAttribute("memberType", paramsMemberType);
			return "memberTypeEdit";
		}
		
		return "infoTip";
	}
	
	/**
	 * @Title: delMemberTypes
	 * @Description: 删除会员级别
	 * @return String
	 */
	public String delMemberTypes(){
		try {
			 //删除会员级别
			adminManager.delMemberTypes(paramsMemberType);
			
			setSuccessTip("删除会员级别成功", "Admin_listMemberTypes.action");
		} catch (Exception e) {
			setErrorTip("删除会员级别异常", "Admin_listMemberTypes.action");
		}
		
		return "infoTip";
	}
	
	/**
	 * @Title: listMembers
	 * @Description: 查询会员
	 * @return String
	 */
	public String listMembers(){
		try {
			if (paramsMember==null) {
				paramsMember = new Member();
			}
			//设置分页信息
			setPagination(paramsMember);
			int[] sum={0};
			List<Member> members = adminManager.listMembers(paramsMember,sum); 
			
			Param.setAttribute("members", members);
			setTotalCount(sum[0]);
			
			//查询会员级别
			MemberType memberType = new MemberType();
			memberType.setStart(-1);
			List<MemberType> memberTypes = adminManager.listMemberTypes(memberType, null);
			Param.setAttribute("memberTypes", memberTypes);
			
		} catch (Exception e) {
			setErrorTip("查询会员异常", "main.jsp");
			return "infoTip";
		}
		
		return "memberShow";
	}
	
	/**
	 * @Title: addMemberShow
	 * @Description: 显示添加会员页面
	 * @return String
	 */
	public String addMemberShow(){
		//查询会员级别
		MemberType memberType = new MemberType();
		memberType.setStart(-1);
		List<MemberType> memberTypes = adminManager.listMemberTypes(memberType, null);
		Param.setAttribute("memberTypes", memberTypes);
		
		return "memberEdit";
	}
	
	/**
	 * @Title: addMember
	 * @Description: 添加会员
	 * @return String
	 */
	public String addMember(){
		try {
			//检查会员编号是否存在
			Member member = new Member();
			member.setMember_no(paramsMember.getMember_no());
			member = adminManager.queryMember(member);
			if (member!=null) {
				tip="失败，该会员编号已经存在！";
				Param.setAttribute("member", paramsMember);
				return "memberEdit";
			}
			
			 //添加会员
			adminManager.addMember(paramsMember);

			setSuccessTip("添加会员成功", "Admin_listMembers.action");
		} catch (Exception e) {
			setErrorTip("添加会员异常", "Admin_listMembers.action");
		}
		return "infoTip";
	}
	
	/**
	 * @Title: queryMember
	 * @Description: 查询会员详情
	 * @return String
	 */
	public String queryMember(){
		try {
			 //得到会员
			Member member = adminManager.queryMember(paramsMember);
			Param.setAttribute("member", member);
			
		} catch (Exception e) {
			setErrorTip("查询会员异常", "Admin_listMembers.action");
			return "infoTip";
		}
		
		return "memberDetail";
	}
	 
	/**
	 * @Title: editMember
	 * @Description: 编辑会员
	 * @return String
	 */
	public String editMember(){
		try {
			 //得到会员
			Member member = adminManager.queryMember(paramsMember);
			Param.setAttribute("member", member);
			
			//查询会员级别
			MemberType memberType = new MemberType();
			memberType.setStart(-1);
			List<MemberType> memberTypes = adminManager.listMemberTypes(memberType, null);
			Param.setAttribute("memberTypes", memberTypes);
			
		} catch (Exception e) {
			setErrorTip("查询会员异常", "Admin_listMembers.action");
			return "infoTip";
		}
		
		return "memberEdit";
	}
	
	/**
	 * @Title: saveMember
	 * @Description: 保存编辑会员
	 * @return String
	 */
	public String saveMember(){
		try {
			 //保存编辑会员
			adminManager.updateMember(paramsMember);
			
		} catch (Exception e) {
			tip="编辑失败";
			Param.setAttribute("member", paramsMember);
			
			//查询会员级别
			MemberType memberType = new MemberType();
			memberType.setStart(-1);
			List<MemberType> memberTypes = adminManager.listMemberTypes(memberType, null);
			Param.setAttribute("memberTypes", memberTypes);
			
			return "memberEdit";
		}
		setSuccessTip("编辑会员成功", "Admin_listMembers.action");
		return "infoTip";
	}
	
	/**
	 * @Title: delMembers
	 * @Description: 删除会员
	 * @return String
	 */
	public String delMembers(){
		try {
			 //删除会员
			adminManager.delMembers(paramsMember);

			setSuccessTip("删除会员成功", "Admin_listMembers.action");
		} catch (Exception e) {
			setErrorTip("删除会员异常", "Admin_listMembers.action");
		}
		return "infoTip";
	}
	
	/**
	 * @Title: listOrderss
	 * @Description: 查询客房预定
	 * @return String
	 */
	public String listOrderss(){
		try {
			if (paramsOrders==null) {
				paramsOrders = new Orders();
			}
			//设置分页信息
			setPagination(paramsOrders);
			//总的条数
			int[] sum={0};
			//查询客房预定列表
			paramsOrders.setOrders_flags("1,2,4");
			List<Orders> orderss = adminManager.listOrderss(paramsOrders,sum); 
			Param.setAttribute("orderss", orderss);
			setTotalCount(sum[0]);
			
			//查询客房类型
			HouseType houseType = new HouseType();
			houseType.setStart(-1);
			List<HouseType> houseTypes = adminManager.listHouseTypes(houseType, null);
			Param.setAttribute("houseTypes", houseTypes);
			
		} catch (Exception e) {
			setErrorTip("查询客房预定异常", "main.jsp");
			return "infoTip";
		}
		
		return "ordersShow";
	}
	
	/**
	 * @Title: addOrdersShow
	 * @Description: 显示添加客房预定页面
	 * @return String
	 */
	public String addOrdersShow(){
		//查询预定人
		Member member = new Member();
		member.setStart(-1);
		List<Member> members = adminManager.listMembers(member, null);
		if (members==null) {
			members = new ArrayList<Member>();
		}
		Param.setAttribute("members", members);
		
		//查询预定的客房
		House house = new House();
		house.setStart(-1);
		house.setHouse_flag(1);
		List<House> houses = adminManager.listHouses(house, null);
		if (houses==null) {
			houses = new ArrayList<House>();
		}
		Param.setAttribute("houses", houses);
		
		return "ordersEdit";
	}
	
	/**
	 * @Title: addOrders
	 * @Description: 添加客房预定
	 * @return String
	 */
	public String addOrders(){
		try {
			//添加客房预定
			adminManager.addOrders(paramsOrders);
			
			setSuccessTip("客房预定成功", "Admin_listOrderss.action");
		} catch (Exception e) {
			setErrorTip("客房预定异常", "Admin_listOrderss.action");
		}
		
		return "infoTip";
	} 
	
	/**
	 * @Title: cancelOrders
	 * @Description: 取消客房预定
	 * @return String
	 */
	public String cancelOrders(){
		try {
			//确认客房预定
			paramsOrders.setOrders_flag(4);
			adminManager.updateOrders(paramsOrders);
			
			setSuccessTip("取消客房预定成功", "Admin_listOrderss.action");
		} catch (Exception e) {
			setErrorTip("取消客房预定异常", "Admin_listOrderss.action");
		}
		
		return "infoTip";
	} 
	
	/**
	 * @Title: confirmOrders
	 * @Description: 确认客房预定
	 * @return String
	 */
	public String confirmOrders(){
		try {
			//确认客房预定
			paramsOrders.setOrders_flag(2);
			adminManager.updateOrders(paramsOrders);
			
			setSuccessTip("确认客房预定成功", "Admin_listOrderss.action");
		} catch (Exception e) {
			setErrorTip("确认客房预定异常", "Admin_listOrderss.action");
		}
		
		return "infoTip";
	} 
	
	/**
	 * @Title: delOrderss
	 * @Description: 删除客房预定
	 * @return String
	 */
	public String delOrderss(){
		try {
			 //删除客房预定
			adminManager.delOrderss(paramsOrders);
			
			setSuccessTip("删除客房预定成功", "Admin_listOrderss.action");
		} catch (Exception e) {
			setErrorTip("删除客房预定异常", "Admin_listOrderss.action");
		}
		
		return "infoTip";
	}
	
	/**
	 * @Title: listStays
	 * @Description: 查询入住记录
	 * @return String
	 */
	public String listStays(){
		try {
			if (paramsStay==null) {
				paramsStay = new Stay();
			}
			//设置分页信息
			setPagination(paramsStay);
			//总的条数
			int[] sum={0};
			//查询入住记录列表
			paramsStay.setStay_flag(1);
			List<Stay> stays = adminManager.listStays(paramsStay,sum); 
			Param.setAttribute("stays", stays);
			setTotalCount(sum[0]);
			
			//查询客房类型
			HouseType houseType = new HouseType();
			houseType.setStart(-1);
			List<HouseType> houseTypes = adminManager.listHouseTypes(houseType, null);
			Param.setAttribute("houseTypes", houseTypes);
			
		} catch (Exception e) {
			setErrorTip("查询入住记录异常", "main.jsp");
			return "infoTip";
		}
		
		return "stayShow";
	}
	
	/**
	 * @Title: listStayHistorys
	 * @Description: 查询入住历史记录
	 * @return String
	 */
	public String listStayHistorys(){
		try {
			if (paramsStay==null) {
				paramsStay = new Stay();
			}
			//设置分页信息
			setPagination(paramsStay);
			//总的条数
			int[] sum={0};
			//查询入住记录列表
			paramsStay.setStay_flag(2);
			List<Stay> stays = adminManager.listStays(paramsStay,sum); 
			Param.setAttribute("stays", stays);
			setTotalCount(sum[0]);
			
			//查询客房类型
			HouseType houseType = new HouseType();
			houseType.setStart(-1);
			List<HouseType> houseTypes = adminManager.listHouseTypes(houseType, null);
			Param.setAttribute("houseTypes", houseTypes);
			
		} catch (Exception e) {
			setErrorTip("查询入住历史记录异常", "main.jsp");
			return "infoTip";
		}
		
		return "stayHistoryShow";
	}
	
	/**
	 * @Title: addStayShow
	 * @Description: 会员入住
	 * @return String
	 */
	public String addStayShow(){
		try {
			 //得到预定情况
			if (paramsOrders!=null) {
				Orders orders = adminManager.queryOrders(paramsOrders);
				Param.setAttribute("orders", orders);
			}
			
			//查询预定的客房
			House house = new House();
			house.setStart(-1);
			house.setHouse_flag(1);
			List<House> houses = adminManager.listHouses(house, null);
			if (houses==null) {
				houses = new ArrayList<House>();
			}
			Param.setAttribute("houses", houses);
			
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		
		return "stayEdit";
	}
	
	/**
	 * @Title: addStay
	 * @Description: 添加入住记录
	 * @return String
	 */
	public String addStay(){
		try {
			//添加入住记录
			adminManager.updateOrdersStay(paramsOrders);
			
			setSuccessTip("客户入住成功", "Admin_listStays.action");
		} catch (Exception e) {
			e.printStackTrace();
			setErrorTip("客户入住异常", "Admin_listStays.action");
		}
		
		return "infoTip";
	} 
	
	/**
	 * @Title: outStayShow
	 * @Description: 退房
	 * @return String
	 */
	public String outStayShow(){
		try {
			//退房
			Stay stay = adminManager.queryStay(paramsStay);
			Param.setAttribute("stay", stay);
			
		} catch (Exception e) {
			setErrorTip("查询客户入住记录异常", "Admin_listStays.action");
			return "infoTip";
		}
		
		return "stayOut";
	} 
	
	/**
	 * @Title: outStay
	 * @Description: 退房
	 * @return String
	 */
	public String outStay(){
		try {
			//退房
			paramsStay.setStay_flag(2);
			adminManager.updateStay(paramsStay);
			
			setSuccessTip("退房成功", "Admin_listStays.action");
		} catch (Exception e) {
			setErrorTip("退房异常", "Admin_listStays.action");
		}
		
		return "infoTip";
	} 
	
	/**
	 * @Title: delStays
	 * @Description: 删除入住记录
	 * @return String
	 */
	public String delStays(){
		try {
			 //删除入住记录
			adminManager.delStays(paramsStay);
			
			setSuccessTip("删除入住记录成功", "Admin_listStays.action");
		} catch (Exception e) {
			setErrorTip("删除入住记录异常", "Admin_listStays.action");
		}
		
		return "infoTip";
	}
	
	private boolean validateAdmin(){
		User admin = (User)Param.getSession("admin");
		if (admin!=null) {
			return true;
		}else {
			return false;
		}
	}
	
	private void setErrorTip(String tip,String url){
		Param.setAttribute("tipType", "error");
		Param.setAttribute("tip", tip);
		Param.setAttribute("url1", url);
		Param.setAttribute("value1", "确 定");
	}
	
	private void setSuccessTip(String tip,String url){
		Param.setAttribute("tipType", "success");
		Param.setAttribute("tip", tip);
		Param.setAttribute("url1", url);
		Param.setAttribute("value1", "确 定");
	}

	public AdminManager getAdminManager() {
		return adminManager;
	}

	public User getParamsUser() {
		return paramsUser;
	}

	public House getParamsHouse() {
		return paramsHouse;
	}

	public HouseType getParamsHouseType() {
		return paramsHouseType;
	}

	public Member getParamsMember() {
		return paramsMember;
	}

	public MemberType getParamsMemberType() {
		return paramsMemberType;
	}

	public Orders getParamsOrders() {
		return paramsOrders;
	}

	public Stay getParamsStay() {
		return paramsStay;
	}

	public String getTip() {
		return tip;
	}

	public void setAdminManager(AdminManager adminManager) {
		this.adminManager = adminManager;
	}

	public void setParamsUser(User paramsUser) {
		this.paramsUser = paramsUser;
	}

	public void setParamsHouse(House paramsHouse) {
		this.paramsHouse = paramsHouse;
	}

	public void setParamsHouseType(HouseType paramsHouseType) {
		this.paramsHouseType = paramsHouseType;
	}

	public void setParamsMember(Member paramsMember) {
		this.paramsMember = paramsMember;
	}

	public void setParamsMemberType(MemberType paramsMemberType) {
		this.paramsMemberType = paramsMemberType;
	}

	public void setParamsOrders(Orders paramsOrders) {
		this.paramsOrders = paramsOrders;
	}

	public void setParamsStay(Stay paramsStay) {
		this.paramsStay = paramsStay;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	 
}
