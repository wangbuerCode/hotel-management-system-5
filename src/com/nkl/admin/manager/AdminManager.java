package com.nkl.admin.manager;

import java.sql.Connection;
import java.util.List;

import com.nkl.common.dao.BaseDao;
import com.nkl.common.util.DateUtil;
import com.nkl.common.util.Md5;
import com.nkl.common.util.StringUtil;
import com.nkl.page.dao.HouseDao;
import com.nkl.page.dao.HouseTypeDao;
import com.nkl.page.dao.MemberDao;
import com.nkl.page.dao.MemberTypeDao;
import com.nkl.page.dao.OrdersDao;
import com.nkl.page.dao.StayDao;
import com.nkl.page.dao.UserDao;
import com.nkl.page.domain.House;
import com.nkl.page.domain.HouseType;
import com.nkl.page.domain.Member;
import com.nkl.page.domain.MemberType;
import com.nkl.page.domain.Orders;
import com.nkl.page.domain.Stay;
import com.nkl.page.domain.User;

public class AdminManager {

	UserDao userDao = new UserDao();
	MemberDao memberDao = new MemberDao();
	MemberTypeDao memberTypeDao = new MemberTypeDao();
	HouseDao houseDao = new HouseDao();
	HouseTypeDao houseTypeDao = new HouseTypeDao();
	OrdersDao ordersDao = new OrdersDao();
	StayDao stayDao = new StayDao();
	
	
	/**
	 * @Title: listUsers
	 * @Description: 用户查询
	 * @param user
	 * @return List<User>
	 */
	public List<User>  listUsers(User user,int[] sum){
		Connection conn = BaseDao.getConnection();
		if (sum!=null) {
			sum[0] = userDao.listUsersCount(user, conn);
		}
		List<User> users = userDao.listUsers(user,conn);
		
		BaseDao.closeDB(null, null, conn);
		return users;
	}
	
	/**
	 * @Title: getUser
	 * @Description: 用户查询
	 * @param user
	 * @return User
	 */
	public User  getUser(User user){
		Connection conn = BaseDao.getConnection();
		User _user = userDao.getUser(user, conn);
		BaseDao.closeDB(null, null, conn);
		return _user;
	}
	 
	/**
	 * @Title: addUser
	 * @Description: 添加用户
	 * @param user
	 * @return void
	 */
	public void  addUser(User user){
		Connection conn = BaseDao.getConnection();
		if (!StringUtil.isEmptyString(user.getUser_pass())) {
			user.setUser_pass(Md5.makeMd5(user.getUser_pass()));
		}
		userDao.addUser(user, conn);
		BaseDao.closeDB(null, null, conn);
	}
	
	/**
	 * @Title: updateUser
	 * @Description: 更新用户信息
	 * @param user
	 * @return void
	 */
	public void  updateUser(User user){
		Connection conn = BaseDao.getConnection();
		if (!StringUtil.isEmptyString(user.getUser_pass())) {
			user.setUser_pass(Md5.makeMd5(user.getUser_pass()));
		}
		userDao.updateUser(user, conn);
		BaseDao.closeDB(null, null, conn);
	}
	
	/**
	 * @Title: delUsers
	 * @Description: 删除用户信息
	 * @param user
	 * @return void
	 */
	public void  delUsers(User user){
		Connection conn = BaseDao.getConnection();
		userDao.delUsers(user.getIds().split(","), conn);
		BaseDao.closeDB(null, null, conn);
	}
	
	/**
	 * @Title: listMemberTypes
	 * @Description: 会员类型查询
	 * @param memberType
	 * @return List<MemberType>
	 */
	public List<MemberType> listMemberTypes(MemberType memberType, int[] sum) {
		Connection conn = BaseDao.getConnection();
		if (sum != null) {
			sum[0] = memberTypeDao.listMemberTypesCount(memberType, conn);
		}
		List<MemberType> memberTypes = memberTypeDao.listMemberTypes(memberType, conn);

		BaseDao.closeDB(null, null, conn);
		return memberTypes;
	}

	/**
	 * @Title: queryMemberType
	 * @Description: 会员类型查询
	 * @param memberType
	 * @return MemberType
	 */
	public MemberType queryMemberType(MemberType memberType) {
		Connection conn = BaseDao.getConnection();
		MemberType _memberType = memberTypeDao.getMemberType(memberType, conn);
		BaseDao.closeDB(null, null, conn);
		return _memberType;
	}

	/**
	 * @Title: addMemberType
	 * @Description: 添加会员类型
	 * @param memberType
	 * @return void
	 */
	public void addMemberType(MemberType memberType) {
		Connection conn = BaseDao.getConnection();
		memberTypeDao.addMemberType(memberType, conn);
		BaseDao.closeDB(null, null, conn);
	}

	/**
	 * @Title: updateMemberType
	 * @Description: 更新会员类型信息
	 * @param memberType
	 * @return void
	 */
	public void updateMemberType(MemberType memberType) {
		Connection conn = BaseDao.getConnection();
		memberTypeDao.updateMemberType(memberType, conn);
		BaseDao.closeDB(null, null, conn);
	}

	/**
	 * @Title: delMemberType
	 * @Description: 删除会员类型信息
	 * @param memberType
	 * @return void
	 */
	public void delMemberTypes(MemberType memberType) {
		Connection conn = BaseDao.getConnection();
		memberTypeDao.delMemberTypes(memberType.getIds().split(","), conn);
		BaseDao.closeDB(null, null, conn);
	}
	
	/**
	 * @Title: listMembers
	 * @Description: 会员查询
	 * @param member
	 * @return List<Member>
	 */
	public List<Member> listMembers(Member member, int[] sum) {
		Connection conn = BaseDao.getConnection();
		if (sum != null) {
			sum[0] = memberDao.listMembersCount(member, conn);
		}
		List<Member> members = memberDao.listMembers(member, conn);

		BaseDao.closeDB(null, null, conn);
		return members;
	}

	/**
	 * @Title: queryMember
	 * @Description: 会员查询
	 * @param member
	 * @return Member
	 */
	public Member queryMember(Member member) {
		Connection conn = BaseDao.getConnection();
		Member _member = memberDao.getMember(member, conn);
		BaseDao.closeDB(null, null, conn);
		return _member;
	}

	/**
	 * @Title: addMember
	 * @Description: 添加会员
	 * @param member
	 * @return void
	 */
	public void addMember(Member member) {
		Connection conn = BaseDao.getConnection();
		member.setMember_date(DateUtil.getCurDate());
		memberDao.addMember(member, conn);
		BaseDao.closeDB(null, null, conn);
	}

	/**
	 * @Title: updateMember
	 * @Description: 更新会员信息
	 * @param member
	 * @return void
	 */
	public void updateMember(Member member) {
		Connection conn = BaseDao.getConnection();
		memberDao.updateMember(member, conn);
		BaseDao.closeDB(null, null, conn);
	}

	/**
	 * @Title: delMember
	 * @Description: 删除会员信息
	 * @param member
	 * @return void
	 */
	public void delMembers(Member member) {
		Connection conn = BaseDao.getConnection();
		memberDao.delMembers(member.getIds().split(","), conn);
		BaseDao.closeDB(null, null, conn);
	}
	
	/**
	 * @Title: listHouseTypes
	 * @Description: 客房类型查询
	 * @param houseType
	 * @return List<HouseType>
	 */
	public List<HouseType> listHouseTypes(HouseType houseType, int[] sum) {
		Connection conn = BaseDao.getConnection();
		if (sum != null) {
			sum[0] = houseTypeDao.listHouseTypesCount(houseType, conn);
		}
		List<HouseType> houseTypes = houseTypeDao.listHouseTypes(houseType, conn);

		BaseDao.closeDB(null, null, conn);
		return houseTypes;
	}

	/**
	 * @Title: queryHouseType
	 * @Description: 客房类型查询
	 * @param houseType
	 * @return HouseType
	 */
	public HouseType queryHouseType(HouseType houseType) {
		Connection conn = BaseDao.getConnection();
		HouseType _houseType = houseTypeDao.getHouseType(houseType, conn);
		BaseDao.closeDB(null, null, conn);
		return _houseType;
	}

	/**
	 * @Title: addHouseType
	 * @Description: 添加客房类型
	 * @param houseType
	 * @return void
	 */
	public void addHouseType(HouseType houseType) {
		Connection conn = BaseDao.getConnection();
		houseTypeDao.addHouseType(houseType, conn);
		BaseDao.closeDB(null, null, conn);
	}

	/**
	 * @Title: updateHouseType
	 * @Description: 更新客房类型信息
	 * @param houseType
	 * @return void
	 */
	public void updateHouseType(HouseType houseType) {
		Connection conn = BaseDao.getConnection();
		houseTypeDao.updateHouseType(houseType, conn);
		BaseDao.closeDB(null, null, conn);
	}

	/**
	 * @Title: delHouseType
	 * @Description: 删除客房类型信息
	 * @param houseType
	 * @return void
	 */
	public void delHouseTypes(HouseType houseType) {
		Connection conn = BaseDao.getConnection();
		houseTypeDao.delHouseTypes(houseType.getIds().split(","), conn);
		BaseDao.closeDB(null, null, conn);
	}
	
	/**
	 * @Title: listHouses
	 * @Description: 客房查询
	 * @param house
	 * @return List<House>
	 */
	public List<House> listHouses(House house, int[] sum) {
		Connection conn = BaseDao.getConnection();
		if (sum != null) {
			sum[0] = houseDao.listHousesCount(house, conn);
		}
		List<House> houses = houseDao.listHouses(house, conn);

		BaseDao.closeDB(null, null, conn);
		return houses;
	}

	/**
	 * @Title: queryHouse
	 * @Description: 客房查询
	 * @param house
	 * @return House
	 */
	public House queryHouse(House house) {
		Connection conn = BaseDao.getConnection();
		House _house = houseDao.getHouse(house, conn);
		BaseDao.closeDB(null, null, conn);
		return _house;
	}

	/**
	 * @Title: addHouse
	 * @Description: 添加客房
	 * @param house
	 * @return void
	 */
	public void addHouse(House house) {
		Connection conn = BaseDao.getConnection();
		houseDao.addHouse(house, conn);
		BaseDao.closeDB(null, null, conn);
	}

	/**
	 * @Title: updateHouse
	 * @Description: 更新客房信息
	 * @param house
	 * @return void
	 */
	public void updateHouse(House house) {
		Connection conn = BaseDao.getConnection();
		houseDao.updateHouse(house, conn);
		BaseDao.closeDB(null, null, conn);
	}

	/**
	 * @Title: delHouse
	 * @Description: 删除客房信息
	 * @param house
	 * @return void
	 */
	public void delHouses(House house) {
		Connection conn = BaseDao.getConnection();
		houseDao.delHouses(house.getIds().split(","), conn);
		BaseDao.closeDB(null, null, conn);
	}
	
	/**
	 * @Title: listOrderss
	 * @Description: 客房预定查询
	 * @param orders
	 * @return List<Orders>
	 */
	public List<Orders>  listOrderss(Orders orders,int[] sum){
		Connection conn = BaseDao.getConnection();
		if (sum!=null) {
			sum[0] = ordersDao.listOrderssCount(orders, conn);
		}
		List<Orders> orderss = ordersDao.listOrderss(orders,conn);
		
		BaseDao.closeDB(null, null, conn);
		return orderss;
	}
	
	/**
	 * @Title: queryOrders
	 * @Description: 客房预定查询
	 * @param orders
	 * @return Orders
	 */
	public Orders  queryOrders(Orders orders){
		Connection conn = BaseDao.getConnection();
		Orders _orders = ordersDao.getOrders(orders, conn);
		BaseDao.closeDB(null, null, conn);
		return _orders;
	}
	 
	/**
	 * @Title: addOrders
	 * @Description: 添加客房预定
	 * @param orders
	 * @return void
	 */
	public void addOrders(Orders orders) {
		Connection conn = BaseDao.getConnection();
		//添加客房预定
		orders.setOrders_date(DateUtil.getCurDate());
		orders.setOrders_flag(1);//未确认
		ordersDao.addOrders(orders, conn);
		
		BaseDao.closeDB(null, null, conn);
	}
	
	/**
	 * @Title: updateOrders
	 * @Description: 更新客房预定
	 * @param orders
	 * @return void
	 */
	public void updateOrders(Orders orders) {
		Connection conn = BaseDao.getConnection();
		//更新客房预定
		ordersDao.updateOrders(orders, conn);
		if (orders.getOrders_flag()==2) {//未入住
			//更新客房已预订
			orders = ordersDao.getOrders(orders, conn);
			House house = new House();
			house.setHouse_id(orders.getHouse_id());
			house.setHouse_flag(2);//已预订
			houseDao.updateHouse(house, conn);
		}
		
		BaseDao.closeDB(null, null, conn);
	}
	
	/**
	 * @Title: updateOrdersStay
	 * @Description: 更新客房入住
	 * @param orders
	 * @return void
	 */
	public void updateOrdersStay(Orders orders) {
		Connection conn = BaseDao.getConnection();
		if (orders.getOrders_id()!=0) {
			//更新预定状态-已入住
			orders.setOrders_flag(3);
			ordersDao.updateOrders(orders, conn);
		}
		
		//更新客房已入住
		House house = new House();
		house.setHouse_id(orders.getHouse_id());
		house = houseDao.getHouse(house, conn);
		house.setHouse_flag(4);//已入住
		houseDao.updateHouse(house, conn);
		
		//办理入住
		Stay stay = new Stay(); 
		stay.setHouse_id(orders.getHouse_id());
		stay.setMember_id(orders.getMember_id());
		stay.setMember_name(orders.getMember_name());
		stay.setMember_card(orders.getMember_card());
		stay.setMember_phone(orders.getMember_phone());
		stay.setStay_date(orders.getStay_date());
		stay.setLeave_date(orders.getLeave_date());
		stay.setStay_money(orders.getStay_money());
		stay.setStay_flag(1);//入住
		stayDao.addStay(stay, conn);
		
		BaseDao.closeDB(null, null, conn);
	}
	
	
	/**
	 * @Title: delOrderss
	 * @Description: 删除客房预定信息
	 * @param orders
	 * @return void
	 */
	public void  delOrderss(Orders orders){
		Connection conn = BaseDao.getConnection();
		houseDao.updateHouseByDelOrdersIds(orders.getIds().split(","), conn);
		ordersDao.delOrderss(orders.getIds().split(","), conn);
		BaseDao.closeDB(null, null, conn);
	}
	
	/**
	 * @Title: listStays
	 * @Description: 入住记录查询
	 * @param stay
	 * @return List<Stay>
	 */
	public List<Stay>  listStays(Stay stay,int[] sum){
		Connection conn = BaseDao.getConnection();
		if (sum!=null) {
			sum[0] = stayDao.listStaysCount(stay, conn);
		}
		List<Stay> stays = stayDao.listStays(stay,conn);
		
		BaseDao.closeDB(null, null, conn);
		return stays;
	}
	
	/**
	 * @Title: queryStay
	 * @Description: 入住记录查询
	 * @param stay
	 * @return Stay
	 */
	public Stay  queryStay(Stay stay){
		Connection conn = BaseDao.getConnection();
		Stay _stay = stayDao.getStay(stay, conn);
		BaseDao.closeDB(null, null, conn);
		return _stay;
	}
	 
	/**
	 * @Title: addStay
	 * @Description: 添加入住记录
	 * @param stay
	 * @return void
	 */
	public void addStay(Stay stay) {
		Connection conn = BaseDao.getConnection();
		//添加入住记录
		stay.setStay_date(DateUtil.getCurDate());
		stay.setStay_flag(1);//未确认
		stayDao.addStay(stay, conn);
		
		BaseDao.closeDB(null, null, conn);
	}
	
	/**
	 * @Title: updateStay
	 * @Description: 更新入住记录-退房（传递House_id）
	 * @param stay
	 * @return void
	 */
	public void updateStay(Stay stay) {
		Connection conn = BaseDao.getConnection();
		//更新入住记录
		stay.setStay_flag(2);//退房
		stayDao.updateStay(stay, conn);
		
		if (stay.getStay_flag()==2) {//退房
			//更新客房待清理
			House house = new House();
			house.setHouse_id(stay.getHouse_id());
			house.setHouse_flag(5);//待清理
			houseDao.updateHouse(house, conn);
		}
		
		BaseDao.closeDB(null, null, conn);
	}
	
	
	/**
	 * @Title: delStays
	 * @Description: 删除入住记录信息
	 * @param stay
	 * @return void
	 */
	public void  delStays(Stay stay){
		Connection conn = BaseDao.getConnection();
		houseDao.updateHouseByDelStayIds(stay.getIds().split(","), conn);
		stayDao.delStays(stay.getIds().split(","), conn);
		BaseDao.closeDB(null, null, conn);
	}
	
}
